
#Don't have spaces in your file names (why would you??) and this won't break on you :)

#Executable name
EXE := exe

#Directories
SRC_DIR := src
INC_DIR := include
BLD_DIR := build
DEP_DIR := $(BLD_DIR)/dep

#File extensions
SRC_EXT := .s
INC_EXT := .mac
OBJ_EXT := .o
DEP_EXT := .d

#Assembler
AS := nasm
AS_FLAGS := -f elf64 -I$(INC_DIR)/

#Linker 
LD := ld
LD_FLAGS := 

#For use in looping rules
SRC := $(SRC_DIR)/%$(SRC_EXT)
OBJ := $(BLD_DIR)/%$(OBJ_EXT)
DEP := $(DEP_DIR)/%$(DEP_EXT)

#For use in non-looping rules
SRC_FILES := $(wildcard $(SRC_DIR)/*$(SRC_EXT))
SRC_FILENAMES := $(notdir $(SRC_FILES))
INC_FILES := $(wildcard $(INC_DIR)/*$(INC_EXT))
OBJ_FILES := $(patsubst %$(SRC_EXT), $(BLD_DIR)/%$(OBJ_EXT), $(SRC_FILENAMES))
DEP_FILES := $(patsubst %$(SRC_EXT), $(DEP_DIR)/%$(DEP_EXT), $(SRC_FILENAMES))


#Rules:
#Link object files into executable
$(EXE) : $(OBJ_FILES)
	$(LD) $(LD_FLAGS) $(OBJ_FILES) -o $(EXE)

#Compile object files
$(OBJ) : $(SRC)
	mkdir -p  $(BLD_DIR)
	$(AS) $(AS_FLAGS) $< -o $@

#Generate dependency files
$(DEP) : $(SRC)
	mkdir -p $(DEP_DIR)
	$(AS) $(AS_FLAGS) $(SRC_DIR)/$*$(SRC_EXT) -M -MF $@ -MT "$(DEP_DIR)/$*$(DEP_EXT) $(BLD_DIR)/$*$(OBJ_EXT)"

#Include dependency files
-include $(DEP_FILES)

.PHONY : clean
clean :
	rm -rf $(BLD_DIR)
	rm $(EXE)
